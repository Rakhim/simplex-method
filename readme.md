SimplexMethod looks for "input.txt" file with the specified format:

{
  "type": "min",
  "target": [0, 2, -1, 7, 11, 5],
  "constraints": {
    "lhs": [
      [ 2, 0, 5,  0, 8],
      [-3, 6, 2, -2, 0]
    ],
    "rel": ["eq", "le"],
    "rhs": [12, 5]
  }
}


initial_task.txt example:

{
  "type": "min",
  "target": [0, 2, -1, 7, 11, 5],
  "vertex_number": 5,
  "value": 1
}

Пример запуска:
java -Xmx2048m -jar target/SimplexMethod-1.0-SNAPSHOT-jar-with-dependencies.jar
