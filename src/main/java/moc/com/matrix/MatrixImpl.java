package moc.com.matrix;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Raim
 */
public class MatrixImpl implements Matrix {
    private final List<MutableVector> rows;
    private final int m;

    public MatrixImpl(int n, int m) {
        rows = create(n, m);
        this.m = m;
    }

    private MutableVector createZeroList(int m) {
        return new MutableVector(new ArrayList<>(Collections.nCopies(m, 0.0)));
    }

    private List<MutableVector> create(int n, int m) {
        final List<MutableVector> rows = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            rows.add(createZeroList(m));
        }
        return rows;
    }

    public int width() {
        return m;
    }

    public int height() {
        return rows.size();
    }

    public void set(int i, int j, double x) {
        rows.get(i).set(j, x);
    }

    public double get(int i, int j) {
        return rows.get(i).get(j);
    }

    public Vector row(int i) {
        return rows.get(i);
    }

    public Vector column(int i) {
        final List<Double> list= new ArrayList<>(height());
        for (int k = 0; k < height(); k++) {
            list.add(rows.get(k).get(i));
        }

        return new VectorImpl(list);
    }

    @Override
    public void addRow(MutableVector vector) {
        rows.add(new MutableVector(vector.asList()));
    }

    public List<MutableVector> asRows() {
        return rows;
    }

    public void show(PrintStream out) {
        final int n = rows.size();
        for (int i = 0; i < n; i++) {
            if (i == n - 1) {
                out.print("M");
            } else if (i == n - 2) {
                out.print("F");
            } else {
                out.print(" ");
            }

            for (int j = 0; j < m; j++) {
                out.print(String.format("%8s", String.format("%4.2f", rows.get(i).get(j))));
            }
            out.println();
        }
        out.println();
    }
}
