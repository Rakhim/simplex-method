/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.matrix;

import java.io.PrintStream;
import java.util.List;

/**
 * @author Raim
 */
public interface Matrix {
    int width();

    int height();

    double get(int i, int j);

    Vector row(int i);

    Vector column(int i);

    void addRow(MutableVector vector);

    void show(PrintStream out);

    List<MutableVector> asRows();
}
