package moc.com.matrix;

import java.util.*;

/**
 * @author Raim
 */
public class VectorImpl implements Vector {
    private final List<Double> elements;

    /**
     * Создание вектора по списку
     *
     * @param list
     */
    public VectorImpl(List<Double> list) {
        elements = list;
    }

    @Override
    public double get(int i) {
        return elements.get(i);
    }

    @Override
    public List<Double> asList() {
        return elements;
    }

    @Override
    public int size() {
        return elements.size();
    }


    /**
     * Скалярное произведение
     *
     * @param right
     * @return
     */
    @Override
    public double mul(Vector right) {
        double value = 0.0;
        for (int i = 0; i < elements.size(); i++) {
            value += elements.get(i) * right.get(i);
        }
        return value;
    }

    /**
     * Cложить вектора записав результат в новый вектор
     *
     * @param right
     * @return
     */
    @Override
    public Vector add(Vector right) {
        final List<Double> list = new ArrayList<>();
        for (int i = 0; i < size(); i++) {
            list.add(i, get(i) + right.get(i));
        }

        return new VectorImpl(list);
    }

    @Override
    public String toString() {
        return Arrays.toString(elements.toArray());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VectorImpl vector = (VectorImpl) o;
        return Objects.equals(elements, vector.elements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(elements);
    }

    public static void main(String[] args) {
        final Vector vector1 = new VectorImpl(new ArrayList<>(Arrays.asList(1.,2.,3.,4.,5.,7.)));
        final Vector vector2 = new VectorImpl(new ArrayList<>(Arrays.asList(1.,2.,3.,4.,5.,7.)));
        assert vector1.equals(vector2);
    }
}
