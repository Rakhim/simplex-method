package moc.com.matrix;

import java.util.List;

public interface Vector {

    double get(int i);

    List<Double> asList();

    int size();

    double mul(Vector right);

    Vector add(Vector right);
}
