package moc.com.matrix;

import java.util.*;

/**
 * @author Raim
 */
public class MutableVector implements Vector {
    private final List<Double> elements;

    /**
     * Создать вектор из 0.0 длины n
     *
     * @param n
     */
    public MutableVector(int n) {
        elements = new ArrayList<>(Collections.nCopies(n, 0.0));
    }

    /**
     * Создание вектора по списку
     *
     * @param list
     */
    public MutableVector(List<Double> list) {
        elements = new ArrayList<>(list);
    }

    public MutableVector set(int i, double x) {
        elements.set(i, x);
        return this;
    }

    public double get(int i) {
        return elements.get(i);
    }

    public List<Double> asList() {
        return elements;
    }

    public int size() {
        return elements.size();
    }

    @Override
    public double mul(Vector right) {
        double value = 0.0;
        for (int i = 0; i < elements.size(); i++) {
            value += elements.get(i) * right.get(i);
        }
        return value;
    }

    /**
     * Cложить вектора записав результат в новый вектор
     *
     * @param right
     * @return
     */
    @Override
    public MutableVector add(Vector right) {
        final List<Double> list = new ArrayList<>();
        for (int i = 0; i < size(); i++) {
            list.add(i, get(i) + right.get(i));
        }

        return new MutableVector(list);
    }

    /**
     * Скалярное произведение
     *
     * @param right
     * @return
     */
    public double mul(MutableVector right) {
        double value = 0.0;
        for (int i = 0; i < elements.size(); i++) {
            value += elements.get(i) * right.get(i);
        }
        return value;
    }

    /**
     * Cложить вектора записав результат в новый вектор
     *
     * @param right
     * @return
     */
    public MutableVector add(MutableVector right) {
        final List<Double> list = new ArrayList<>();
        for (int i = 0; i < size(); i++) {
            list.add(i, get(i) + right.get(i));
        }

        return new MutableVector(list);
    }

    public void concat(double value) {
        elements.add(value);
    }

    @Override
    public String toString() {
        return Arrays.toString(elements.toArray());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final MutableVector vector = (MutableVector) o;
        return Objects.equals(elements, vector.elements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(elements);
    }
}
