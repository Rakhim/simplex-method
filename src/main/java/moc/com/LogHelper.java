package moc.com;

public class LogHelper {

    private static LogHelper helper = new LogHelper();

    private LogHelper() {}

    public static LogHelper getHelper() {
        return helper;
    }

    public void info(Object... msgs) {
        for (Object msg : msgs) {
            System.out.print(msg.toString());
        }
        System.out.println();
    }
}
