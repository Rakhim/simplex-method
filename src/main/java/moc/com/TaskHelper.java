package moc.com;

import moc.com.matrix.MutableVector;
import moc.com.matrix.Vector;
import moc.com.mc.GraphConnectivityHelper;
import moc.com.mc.StoerWagnerMinimumCut;
import moc.com.mc.WeightedEdge;
import moc.com.mc.WeightedGraph;
import moc.com.simplexmethod.Function;

import java.util.*;

public class TaskHelper {

    private TaskHelper() {}

    /**
     * Являются ли коэффициенты функций целочисленными
     *
     * @param point -   точка - решение ЛП
     * @return      -   true - являются
     */
    public static boolean isIntegerCoefficients(Vector point) {

        for (Double coefficient : point.asList()) {
            if (!isInteger(coefficient)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Проверить является ли граф из коэффициентов функций связным.
     *
     * @param point         -   точка - решение задачи ЛП
     * @param vertexCount   -   количество вершин
     * @return -   true - связен
     */
    public static boolean isConnected(Vector point, int vertexCount) {
        return GraphConnectivityHelper.isConnected(vertexCount, point.asList());
    }

    /**
     * Создание описания полного графа из коэффицентов функций
     *
     * @param point         -   точка решение задачи ЛП
     * @param vertexCount   -   количестов вершин. (vertexCount * (vertexCount - 1) / 2 == {@link Function#deg()})
     * @return
     */
    public static WeightedGraph toGraph(Vector point, int vertexCount) {

        final WeightedGraph graph = new WeightedGraph();
        final List<Double> weights = point.asList();
        fillGraph(graph, vertexCount, weights);
        return graph;
    }

    public static StoerWagnerMinimumCut minimumCut(Vector point, int vertexCount) {
        return new StoerWagnerMinimumCut(toGraph(point, vertexCount));
    }

    public static MutableVector minimumCutAsVector(StoerWagnerMinimumCut cut, int vertexCount) {
        final Set<Long> vertexes = (Set<Long>) cut.minCut();
        final List<Double> list = new ArrayList<>(edgeCount(vertexCount));
        boolean inCut;
        for (long i = 1; i < vertexCount + 1; i++) {
            for (long j = i + 1; j < vertexCount + 1; j++) {
                inCut = vertexes.contains(i) && !vertexes.contains(j);
                list.add(inCut ? 1. : 0);
            }
        }

        return new MutableVector(list);
    }

    private static int edgeCount(int vertexCount) {
        return (vertexCount * (vertexCount - 1)) / 2;
    }

    private static boolean isInteger(Double value) {
        return value.equals(Double.valueOf(value.intValue()));
    }

    private static void fillGraph(WeightedGraph graph, int vertexCount, List<Double> weights) {
        addVertexes(graph, vertexCount);

        final Iterator<Double> iterator = weights.iterator();
        for (long i = 1; i < vertexCount + 1; i++) {
            for (long j = i + 1; j < vertexCount + 1; j++) {
                addEdge(graph, i, j, iterator.next());
            }
        }
    }

    private static void addVertexes(WeightedGraph graph, int vertexCount) {
        for (long i = 0; i < vertexCount; i++) {
            graph.addVertex(i + 1);
        }
    }

    private static void addEdge(WeightedGraph graph, Long left, Long right, double weight) {
        final WeightedEdge edge = new WeightedEdge("default");
        graph.addEdge(left, right, edge);
        graph.setEdgeWeight(edge, weight);
    }
}
