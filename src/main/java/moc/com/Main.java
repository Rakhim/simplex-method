package moc.com;

import moc.com.matrix.MutableVector;
import moc.com.matrix.Vector;
import moc.com.matrix.VectorImpl;
import moc.com.mc.StoerWagnerMinimumCut;
import moc.com.simplexmethod.*;
import org.apache.commons.math3.optim.linear.NoFeasibleSolutionException;


import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static moc.com.TaskLoadHelper.loadInitialTask;

public class Main {
    private static final LogHelper log = LogHelper.getHelper();

    public static void main(String[] args) {
        try {
            final InitialTask initialTask = loadInitialTask("initial_input.txt");
            final GeneralTask generalTask = initialTask.toTask();
            final Resolution resolution = new Resolution();
            resolution.setSolution(new Solution(0, new VectorImpl(Collections.emptyList())));
            log.info("Начальная задача ЛП: \n", generalTask, "\n");

            int iterations  = 0;
            while (!resolution.isReady()) {
                solve(resolution, generalTask, initialTask.getVertexCount());
                iterations++;
            }

            log.info(String.format("Итерации: %d", iterations));
        } catch (NoFeasibleSolutionException e) {
            log.info("Задача ЛП не имеет решения.");
        } catch (Exception ex) {
            Logger.getLogger(SimplexMethodHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void solve(Resolution resolution, GeneralTask generalTask, int vertexCount) {
        final Solution solution = SimplexMethodHelper.solve(generalTask);
        final Vector point = solution.getPoint();
        final boolean integerCoefficients = TaskHelper.isIntegerCoefficients(point);
        final boolean connected = TaskHelper.isConnected(point, vertexCount);
        final Constraints constraints = generalTask.getConstraints();
        resolution.setSolution(solution);

        if (integerCoefficients && connected) {
            log.info("План целочислен и порождает связный граф. Решение найдено.");
            log.info(solution);

            resolution.setReady(true);
        } else if (integerCoefficients) {
            log.info("План целочислен и порождает не связный граф.");

            if (!addComponent(resolution, constraints, point, vertexCount)) {
                return;
            }

            resolution.setReady(false);
        } else {
            log.info("План не целочислен и порождает не связный граф.");

            final StoerWagnerMinimumCut minimumCut = TaskHelper.minimumCut(point, vertexCount);
            if (minimumCut.minCutWeight() >= 2.0) {
                throw new IllegalStateException("Нет такого отсечения.");
            }

            final MutableVector vector = TaskHelper.minimumCutAsVector(minimumCut, vertexCount);
            if (constraints.hasConstraint(vector, 2)) {
                log.info("Вычисление зациклилось. Добавляемое ограничение уже существует. ");
                resolution.setReady(true);
                return;
            }

            constraints.addConstraint(vector, 2, Relation.GE);
            resolution.setReady(false);
        }
    }

    private static boolean addComponent(Resolution resolution, Constraints constraints, Vector vector, int vertexCount) {
        final List<MutableVector> components = ComponentHelper.extractComponents(vector.asList(), vertexCount);
        log.info("Компонент: ", components.size());
        final double constraintRhs = 2.0;
        for (MutableVector component : components) {
            if (!constraints.hasConstraint(component, constraintRhs)) {
                constraints.addConstraint(component, constraintRhs, Relation.GE);
                log.info("Добавлено новое ограничение: ", component);
                return true;
            } else {
                log.info("Добавляемое ограничение уже существует: ", component);
            }
        }

        log.info("Вычисление зациклилось. Ни одно из ограничении не является новым. ");
        resolution.setReady(true);
        return false;
    }

    private static class Resolution {
        private boolean ready;
        private Solution solution;

        public boolean isReady() {
            return ready;
        }

        public void setReady(boolean ready) {
            this.ready = ready;
        }

        public Solution getSolution() {
            return solution;
        }

        public void setSolution(Solution solution) {
            this.solution = solution;
        }
    }
}
