package moc.com.simplexmethod;

import moc.com.matrix.Matrix;
import moc.com.matrix.Vector;
import moc.com.matrix.VectorImpl;
import org.apache.commons.math3.optim.MaxIter;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.linear.*;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static moc.com.TaskLoadHelper.loadTask;

/**
 * Hello world!
 */
public class SimplexMethodHelper {
    /**
     * Имя файла описания задачи.
     */
    private static final String FILE_NAME = "input.txt";

    /**
     * Для проверки решателя отдельно
     */
    public static void main(String[] args) {
        try {
            final GeneralTask task = loadTask(FILE_NAME);
            final Solution solution = solve(task);
            final Vector point = solution.getPoint();
            final double value = solution.getValue();

        } catch (Exception ex) {
            Logger.getLogger(SimplexMethodHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Решает задачу линейного программирвоания
     * @param task  -   описание задачи
     * @return      -   пара<точка, оптимальное значение>
     */
    public static Solution solve(GeneralTask task) {
        final SimplexSolver solver = new SimplexSolver();
        final PointValuePair pair = solver.optimize(
            new MaxIter(17300),
            getObjectiveFunction(task),
            getConstraints(task),
            getGoalType(task),
            new NonNegativeConstraint(true)
        );

        return new Solution(
            pair.getValue(),
            new VectorImpl(toList(pair.getPoint()))
        );
    }

    private static List<Double> toList(double[] values) {
        final List<Double> list = new ArrayList<>(values.length);
        for (double value : values) {
            list.add(value);
        }
        return list;
    }

    private static double[] toArray(List<Double> values) {
        final double[] array = new double[values.size()];
        int i = 0;
        for (double value : values) {
            array[i++] = value;
        }
        return array;
    }

    private static Relationship getRelationship(Relation relation) {
        final HashMap<Relation, Relationship> map = new HashMap<>();
        map.put(Relation.EQ, Relationship.EQ);
        map.put(Relation.LE, Relationship.LEQ);
        map.put(Relation.GE, Relationship.GEQ);
        return map.get(relation);
    }

    private static LinearObjectiveFunction getObjectiveFunction(GeneralTask task) {
        final Function function = task.getFunction();
        return new LinearObjectiveFunction(
            toArray(function.getCoefficients()),
            function.getConstant()
        );
    }

    private static LinearConstraintSet getConstraints(GeneralTask task) {
        final Constraints constraintDescription = task.getConstraints();
        final Matrix lhs = constraintDescription.getLhs();
        final Vector rhs = constraintDescription.getRhs();
        final List<Relation> relations = constraintDescription.getRelations();
        final Collection<LinearConstraint> constraints = new ArrayList<>();
        for (int i = 0; i < lhs.height(); i++) {
            constraints.add(new LinearConstraint(
                toArray(lhs.row(i).asList()),
                getRelationship(relations.get(i)),
                rhs.get(i)
            ));
        }

        return new LinearConstraintSet(constraints);
    }

    private static GoalType getGoalType(GeneralTask task) {
        final HashMap<TaskType, GoalType> map = new HashMap<>();
        map.put(TaskType.MIN, GoalType.MINIMIZE);
        map.put(TaskType.MAX, GoalType.MAXIMIZE);
        return map.get(task.getType());
    }
}
