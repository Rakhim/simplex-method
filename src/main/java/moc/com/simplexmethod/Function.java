package moc.com.simplexmethod;

import moc.com.matrix.Vector;
import moc.com.matrix.VectorImpl;

import java.util.List;
import java.util.Locale;

/**
 * Представляет собой функцию
 * Вектор -> Действительное число
 * constant     - свободный член
 * coefficients - коэффициент при x[i]
 *
 * @author Raim
 */
public class Function {
    private final double constant;
    private final Vector coefficients;

    public Function(List<Double> coefficients) {
        this.coefficients = new VectorImpl(coefficients.subList(1, coefficients.size()));
        this.constant = coefficients.get(0);
    }

    public double get(int i) {
        return coefficients.get(i);
    }

    public double evaluate(Vector vector) {
        return coefficients.mul(vector) + constant;
    }

    public int deg() {
        return coefficients.size();
    }

    public List<Double> getCoefficients() {
        return coefficients.asList();
    }

    public double getConstant() {
        return constant;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(String.format(Locale.US, "F: %4.4f ", constant));

        for (double x : coefficients.asList()) {
            builder.append(String.format(Locale.US, "%4.4f ", x));
        }

        return builder.toString();
    }
}
