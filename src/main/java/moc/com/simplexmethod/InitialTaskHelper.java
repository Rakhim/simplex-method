package moc.com.simplexmethod;

import moc.com.matrix.Matrix;
import moc.com.matrix.MatrixImpl;
import moc.com.matrix.Vector;
import moc.com.matrix.VectorImpl;

import java.util.ArrayList;
import java.util.List;

public class InitialTaskHelper {

    public static GeneralTask fromInitialTask(InitialTask initialTask) {
        return new GeneralTask(
            TaskType.MIN,
            initialTask.getFunction(),
            getConstraints(initialTask.getVertexCount(), initialTask.getValue())
        );
    }

    private static List<Edge> edges(int n) {
        final List<Edge> list = new ArrayList<Edge>();
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                list.add(new Edge(i + 1, j + 1));
            }
        }
        return list;
    }

    private static void fillncidentMatrix(MatrixImpl matrix, int vertexCount) {
        final List<Edge> edges = edges(vertexCount);
        for (int i = 0; i < vertexCount; i++) {
            for (int j = 0; j < matrix.width(); j++) {
                matrix.set(i, j, edges.get(j).incident(i + 1) ? 1 : 0);
            }
        }
    }

    private static void fillDiagonalMatrix(MatrixImpl matrix, int position) {
        for (int i = 0; i < matrix.width(); i++) {
            matrix.set(position + i, i, 1);
        }
    }

    private static Matrix getConstraintsLhs(int vertexCount) {
        final int edgeCount = vertexCount * (vertexCount - 1) / 2;
        final MatrixImpl matrix = new MatrixImpl(vertexCount + 2 * edgeCount, edgeCount);
        fillncidentMatrix(matrix, vertexCount);
        fillDiagonalMatrix(matrix, vertexCount);
        fillDiagonalMatrix(matrix, vertexCount + edgeCount);
        return matrix;
    }

    private static List<Relation> getRelations(int vertexCount) {
        final int edgeCount = vertexCount * (vertexCount - 1) / 2;
        final List<Relation> list = new ArrayList<Relation>();
        for (int i = 0; i < vertexCount;i++) {
            list.add(Relation.EQ);
        }
        for (int i = 0; i < edgeCount;i++) {
            list.add(Relation.LE);
        }
        for (int i = 0; i < edgeCount;i++) {
            list.add(Relation.GE);
        }
        return list;
    }

    private static Vector getRhs(int vertexCount, int rhsNumber) {
        final int edgeCount = vertexCount * (vertexCount - 1) / 2;
        final List<Double> list = new ArrayList<>();
        for (int i = 0; i < vertexCount; i++) {
            list.add((double) rhsNumber);
        }

        for (int i = 0; i < edgeCount; i++) {
            list.add(1.0);
        }

        for (int i = 0; i < edgeCount; i++) {
            list.add(0.0);
        }
        return new VectorImpl(list);
    }

    private static Constraints getConstraints(int vertexCount, int rhsNumber) {
        return new Constraints(
            getConstraintsLhs(vertexCount),
            getRhs(vertexCount, rhsNumber),
            getRelations(vertexCount)
        );
    }

    private static class Edge {
        int x;
        int y;

        private Edge(int x, int y) {
            this.x = x;
            this.y = y;
        }

        boolean incident(int z) {
            return x == z || y == z;
        }

    }
}
