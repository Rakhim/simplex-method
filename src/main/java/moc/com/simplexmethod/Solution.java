package moc.com.simplexmethod;

import moc.com.matrix.Vector;

public class Solution {
    private final double value;
    private final Vector point;

    public Solution(double value, Vector point) {
        this.value = value;
        this.point = point;
    }

    public double getValue() {
        return value;
    }

    public Vector getPoint() {
        return point;
    }

    @Override
    public String toString() {
        return new StringBuilder()
            .append(String.format("    План: %s\n", point))
            .append(String.format("Значение: %f", value))
            .toString();
    }
}
