package moc.com.simplexmethod;

import moc.com.matrix.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Ограничения задачи.
 */
public class Constraints {
    private final Matrix lhs; /* левая часть системы ограничений*/
    private final MutableVector rhs; /* правая часть системы ограниений*/
    private final List<Relation> relations; /* отношения между частями */

    public Constraints(Matrix lhs, Vector rhs, List<Relation> relations) {
        this.lhs = lhs;
        this.rhs = new MutableVector(rhs.asList());
        this.relations = relations;
    }

    public Constraints(Matrix lhs, MutableVector rhs, List<Relation> relations) {
        this.lhs = lhs;
        this.rhs = new MutableVector(rhs.asList());
        this.relations = relations;
    }

    private Constraints() {
        this(
            new MatrixImpl(0, 0),
            new MutableVector(0),
            new ArrayList<>()
        );
    }

    public Matrix getLhs() {
        return lhs;
    }

    public Vector getRhs() {
        return rhs;
    }

    public List<Relation> getRelations() {
        return relations;
    }

    public void addConstraint(MutableVector vector, double value, Relation relation) {
        lhs.addRow(vector);
        rhs.concat(value);
        relations.add(relation);
    }

    public static void main(String[] args) {
        final Constraints constraints = new Constraints();
        constraints.addConstraint(new MutableVector(Arrays.asList(1.0, 2.0, 3.0)), 5.0, Relation.EQ);
        constraints.addConstraint(new MutableVector(Arrays.asList(2.0, 3.0, 4.0)), 3.0, Relation.GE);
        constraints.addConstraint(new MutableVector(Arrays.asList(5.0, 7.0, 9.0)), 2.0, Relation.LE);

        System.out.println();
    }

    @Override
    public String toString() {
        int i = 0;

        final StringBuilder builder = new StringBuilder();
        for (Vector vector : lhs.asRows()) {
            builder
                .append(vector)
                .append(String.format(" %s", relations.get(i)))
                .append(String.format(" %s", rhs.get(i)))
                .append("\n");

            i++;
        }

        return builder.toString();
    }

    public boolean hasConstraint(Vector vector, double rhs) {
        int i = 0;
        for (Vector row : lhs.asRows()) {
            if (row.equals(vector) && this.rhs.get(i) == rhs) {
                return true;
            }
            i++;
        }

        return false;
    }
}
