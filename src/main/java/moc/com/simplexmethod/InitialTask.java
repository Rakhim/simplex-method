package moc.com.simplexmethod;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import moc.com.serializer.InitialTaskDeserializer;

@JsonDeserialize(using = InitialTaskDeserializer.class)
public class InitialTask {
    private final TaskType taskType;
    private final Function function;
    private final int vertexCount;
    private final int value;

    public InitialTask(TaskType taskType, Function function, int vertexCount, int value) {
        this.taskType = taskType;
        this.function = function;
        this.vertexCount = vertexCount;
        this.value = value;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public Function getFunction() {
        return function;
    }

    public int getVertexCount() {
        return vertexCount;
    }

    public int getValue() {
        return value;
    }

    public GeneralTask toTask() {
        return InitialTaskHelper.fromInitialTask(this);
    }
}
