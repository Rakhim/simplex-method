package moc.com.simplexmethod;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import moc.com.matrix.Vector;
import moc.com.serializer.TaskDeserializer;
import moc.com.matrix.Matrix;

import java.util.List;

/**
 * Представление задачи описанной в файле input.txt
 *
 * @author Raim
 */
@JsonDeserialize(using = TaskDeserializer.class)
public class GeneralTask {
    private final TaskType type;
    private final Function function;
    private final Constraints constraints;

    public GeneralTask(TaskType type, Function function, Constraints constraints) {
        this.type = type;
        this.function = function;
        this.constraints = constraints;
    }

    public TaskType getType() {
        return type;
    }

    public Function getFunction() {
        return function;
    }

    public Matrix getLhs() {
        return constraints.getLhs();
    }

    public Vector getRhs() {
        return constraints.getRhs();
    }

    public List<Relation> getRelations() {
        return constraints.getRelations();
    }

    public Constraints getConstraints() {
        return constraints;
    }

    public String toString() {
        return new StringBuilder()
            .append(function)
            .append(String.format(" -> %s", type))
            .append("\n\n")
            .append(constraints)
            .toString();
    }
}
