/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moc.com.simplexmethod;

/**
 * Тип задачи.
 *
 * @author Raim
 */
public enum TaskType {
    MIN,
    MAX
}
