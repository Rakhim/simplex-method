package moc.com.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import moc.com.matrix.*;
import moc.com.simplexmethod.*;

/**
 * @author Raim
 */
public class TaskDeserializer extends StdDeserializer<GeneralTask> {
    private static String TASK_TYPE_TAG = "type";
    private static String CONSTRAINTS_TAG = "constraints";
    private static String RELATIONS_TAG = "rel";
    private static String RHS_TAG = "rhs";
    private static String LHS_TAG = "lhs";
    private static String TARGET_TAG = "target";

    public TaskDeserializer() {
        this(null);
    }

    public TaskDeserializer(Class<?> clazz) {
        super(clazz);
    }

    private Matrix loadConstraintsLhs(JsonNode constraints) {
        final JsonNode matrix = constraints.get(LHS_TAG);
        final int n = matrix.size();
        final int m = matrix.path(0).size();
        final MatrixImpl constraintsLhs = new MatrixImpl(0, 0);

        for (int i = 0; i < n; i++) {
            final JsonNode rowNode = matrix.path(i);
            final List<Double> rowElements = new ArrayList<>();
            for (int j = 0; j < m; j++) {
                rowElements.add(rowNode.path(j).asDouble());
            }

            constraintsLhs.addRow(new MutableVector(rowElements));
        }

        return constraintsLhs;
    }

    private Vector loadConstraintsRhs(JsonNode constraints) {
        final JsonNode rhs = constraints.get(RHS_TAG);
        final List<Double> constraintsRhs = new ArrayList<Double>();
        for (JsonNode e : rhs) {
            constraintsRhs.add(e.asDouble());
        }
        return new VectorImpl(constraintsRhs);
    }

    private List<Relation> loadConstraintRelations(JsonNode constraints) {
        final JsonNode rhs = constraints.get(RELATIONS_TAG);
        final List<Relation> relations = new ArrayList<Relation>();
        for (JsonNode e : rhs) {
            relations.add(Relation.valueOf(e.asText().toUpperCase()));
        }

        return relations;
    }

    private TaskType loadType(JsonNode node) {
        final JsonNode target = node.get(TASK_TYPE_TAG);
        return TaskType.valueOf(target.asText().toUpperCase());
    }

    private Function loadTargetFunction(JsonNode node) {
        final JsonNode target = node.get(TARGET_TAG);
        final List<Double> coefficients = new ArrayList<Double>();
        for (JsonNode e : target) {
            coefficients.add(e.asDouble());
        }

        return new Function(coefficients);
    }

    @Override
    public GeneralTask deserialize(JsonParser parser, DeserializationContext dc) throws IOException {
        final JsonNode node = parser.getCodec().readTree(parser);
        final JsonNode constraintsNode = node.get(CONSTRAINTS_TAG);
        final Constraints constraints = new Constraints(
                loadConstraintsLhs(constraintsNode),
                loadConstraintsRhs(constraintsNode),
                loadConstraintRelations(constraintsNode)
        );

        return new GeneralTask(
                loadType(node),
                loadTargetFunction(node),
                constraints
        );
    }
}
