package moc.com.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import moc.com.simplexmethod.Function;
import moc.com.simplexmethod.InitialTask;
import moc.com.simplexmethod.TaskType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class InitialTaskDeserializer  extends StdDeserializer<InitialTask> {
    private static String TASK_TYPE_TAG = "type";
    private static String TARGET_TAG = "target";
    private static String VERTEX_COUNT_TAG = "vertex_count";
    private static String VALUE_TAG = "value";

    public InitialTaskDeserializer() {
        this(null);
    }

    public InitialTaskDeserializer(Class<?> clazz) {
        super(clazz);
    }

    private TaskType loadType(JsonNode node) {
        final JsonNode target = node.get(TASK_TYPE_TAG);
        return TaskType.valueOf(target.asText().toUpperCase());
    }

    private Function loadTargetFunction(JsonNode node) {
        final JsonNode target = node.get(TARGET_TAG);
        final List<Double> coefficients = new ArrayList<Double>();
        for (JsonNode e: target) {
            coefficients.add(e.asDouble());
        }

        return new Function(coefficients);
    }

    private int loadIntValue(JsonNode node, String tagName) {
        final JsonNode field = node.get(tagName);
        return field.asInt();
    }

    private int loadVertexCount(JsonNode node) {
        return loadIntValue(node, VERTEX_COUNT_TAG);
    }

    private int loadValue(JsonNode node) {
        return loadIntValue(node, VALUE_TAG);
    }

    public InitialTask deserialize(JsonParser parser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        final JsonNode node = parser.getCodec().readTree(parser);
        return new InitialTask(
            loadType(node),
            loadTargetFunction(node),
            loadVertexCount(node),
            loadValue(node)
        );
    }
}
