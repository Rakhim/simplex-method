package moc.com.mc;

import java.util.*;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jgrapht.Graphs;

import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.graph.SimpleWeightedGraph;


/**
 * Implements the <a href="http://dl.acm.org/citation.cfm?id=263872">Stoer and
 * Wagner minimum cut algorithm</a>. Deterministically computes the minimum cut
 * in O(|V||E| + |V|log|V|) time. This implementation uses Java's PriorityQueue
 * and requires O(|V||E|log|E|) time. M. Stoer and F. Wagner, "A Simple Min-Cut
 * Algorithm", Journal of the ACM, volume 44, number 4. pp 585-591, 1997.
 *
 * @author Robby McKilliam, Ernst de Ridder
 */
public class StoerWagnerMinimumCut<V, E> {
    final SimpleGraph<Set<Long>, WeightedEdge> workingGraph;
    private double bestCutWeight = Double.POSITIVE_INFINITY;
    private Pair<Double, List<WeightedEdge>> bestCutEgdes;
    private Set<Long> bestCut;

    /**
     * Will compute the minimum cut in graph.
     *
     * @param graph graph over which to run algorithm
     * @throws IllegalArgumentException if a negative weight edge is found
     * @throws IllegalArgumentException if graph has less than 2 vertices
     */
    public StoerWagnerMinimumCut(WeightedGraph graph) {
        check(graph);

        workingGraph = new SimpleWeightedGraph<>(WeightedEdge.class);

        final Map<Long, Set<Long>> vertexMap = new HashMap<>();
        copyVertexes(workingGraph, graph, vertexMap);
        copyEdges(workingGraph, graph, vertexMap);

        //arbitrary vertex used to seed the algorithm.
        final Set<Long> a = workingGraph.vertexSet().iterator().next();
        while (workingGraph.vertexSet().size() > 1) {
            minimumCutPhase(a);
        }
    }

    private void check(WeightedGraph graph) {
        if (graph.vertexSet().size() < 2) {
            throw new IllegalArgumentException("Graph has less than 2 vertices");
        }

        for (WeightedEdge e : graph.edgeSet()) {
            if (graph.getEdgeWeight(e) < 0.0) {
                throw new IllegalArgumentException("Negative edge weights not allowed");
            }
        }
    }

    private void copyVertexes(SimpleGraph<Set<Long>, WeightedEdge> toGraph, WeightedGraph graph, Map<Long, Set<Long>> vertexMap) {
        for (Long v : graph.vertexSet()) {
            final Set<Long> list = new HashSet<>();
            list.add(v);
            vertexMap.put(v, list);
            toGraph.addVertex(list);
        }
    }

    private void copyEdges(SimpleGraph<Set<Long>, WeightedEdge> toGraph, WeightedGraph graph, Map<Long, Set<Long>> vertexMap) {
        for (WeightedEdge e : graph.edgeSet()) {
            final Set<Long> sNew = vertexMap.get(graph.getEdgeSource(e));
            final Set<Long> tNew = vertexMap.get(graph.getEdgeTarget(e));
            final WeightedEdge edge = graph.getEdge(graph.getEdgeSource(e), graph.getEdgeTarget(e));
            toGraph.addEdge(sNew, tNew, edge);
            toGraph.setEdgeWeight(edge, graph.getEdgeWeight(e));
        }
    }

    /**
     * Implements the MinimumCutPhase function of Stoer and Wagner
     */
    private void minimumCutPhase(Set<Long> a) {
        final PriorityQueue<VertexAndWeight> queue = new PriorityQueue<>(); // queue contains vertices not in A ordered by max weight of edges to A.
        final Map<Set<Long>, VertexAndWeight> dmap = new HashMap<>(); // Maps vertices to elements of queue

        // Initialize queue
        for (Set<Long> left : workingGraph.vertexSet()) {
            if (left != a) {
                final boolean containsEdge = workingGraph.containsEdge(left, a);
                final WeightedEdge e = workingGraph.getEdge(left, a);
                final Double weight = containsEdge ? workingGraph.getEdgeWeight(e) : 0.0;
                final VertexAndWeight vertexAndWeight = new VertexAndWeight(left, weight, containsEdge);
                queue.add(vertexAndWeight);
                dmap.put(left, vertexAndWeight);
            }
        }


        // Now iteratively update the queue to get the required vertex ordering
        // The last and before last vertices added to A.
        Set<Long> last = a;
        Set<Long> beforelast = null;
        while (!queue.isEmpty()) {
            final Set<Long> vertex = queue.poll().vertex;
            dmap.remove(vertex);

            beforelast = last;
            last = vertex;

            for (WeightedEdge left : workingGraph.edgesOf(vertex)) {
                final Set<Long> right = Graphs.getOppositeVertex(workingGraph, left, vertex);
                if (dmap.containsKey(right)) {
                    final VertexAndWeight vertexAndWeight = dmap.get(right);
                    queue.remove(vertexAndWeight); //this is O(logn) but could be O(1)?
                    vertexAndWeight.active = true;
                    vertexAndWeight.weight += workingGraph.getEdgeWeight(left);
                    queue.add(vertexAndWeight); //this is O(logn) but could be O(1)?
                }
            }
        }

        // Update the best cut
        final Pair<Double, List<WeightedEdge>> vertexWeight = getVertexWeight(last);
        if (vertexWeight.getLeft() < bestCutWeight) {
            bestCutWeight = vertexWeight.getLeft();
            bestCut = last;
            bestCutEgdes = vertexWeight;
        }

        //merge the last added vertices
        mergeVertices(beforelast, last);
    }

    /**
     * Return the weight of the minimum cut
     */
    public double minCutWeight() {
        return bestCutWeight;
    }

    /**
     * Return the weight of the minimum cut
     */
    public Pair<Double, List<WeightedEdge>> minCutEdges() {
        return bestCutEgdes;
    }

    /**
     * Return a set of vertices on one side of the cut
     */
    public Set<Long> minCut() {
        return bestCut;
    }

    private double getEdgeWeight(SimpleGraph<Set<Long>, WeightedEdge> graph, Set<Long> left, Set<Long> right) {
        return graph.containsEdge(left, right) ? workingGraph.getEdgeWeight(workingGraph.getEdge(left, right)) : 0;
    }

    /**
     * Merges vertex t into vertex s, summing the weights as required. Returns
     * the merged vertex and the sum of its weights
     */
    private VertexAndWeight mergeVertices(Set<Long> left, Set<Long> right) {
        final Set<Long> set = new HashSet<>();
        set.addAll(left);
        set.addAll(right);
        workingGraph.addVertex(set);

        //add edges and weights to the combined vertex
        double pathWeight = 0.0;
        for (Set<Long> vertex : workingGraph.vertexSet()) {
            if ((left != vertex) && (right != vertex)) {
                final double temporaryPathWeight = getEdgeWeight(workingGraph, left, vertex) + getEdgeWeight(workingGraph, right, vertex);
                if (temporaryPathWeight > 0.0) {
                    pathWeight += temporaryPathWeight;

                    final WeightedEdge accumulatorEdge = new WeightedEdge("accumulator");
                    workingGraph.addEdge(set, vertex, accumulatorEdge);
                    workingGraph.setEdgeWeight(accumulatorEdge, temporaryPathWeight);
                }
            }
        }

        //remove original vertices
        workingGraph.removeVertex(right);
        workingGraph.removeVertex(left);

        return new VertexAndWeight(set, pathWeight, false);
    }

    /**
     * Compute the sum of the weights entering a vertex
     */
    private Pair<Double, List<WeightedEdge>> getVertexWeight(Set<Long> vertex) {
        Double totalWeight = 0.0;
        final List<WeightedEdge> list = new ArrayList<>();
        for (WeightedEdge edge : workingGraph.edgesOf(vertex)) {
            totalWeight += workingGraph.getEdgeWeight(edge);
            list.add(edge);
        }
        return new ImmutablePair<>(totalWeight, list);
    }


    /**
     * Class for weighted vertices
     */
    protected class VertexAndWeight implements Comparable<VertexAndWeight> {
        public Set<Long> vertex;
        public Double weight;
        public boolean active; // active == neighbour in A

        public VertexAndWeight(Set<Long> v, double w, boolean active) {
            this.vertex = v;
            this.weight = w;
            this.active = active;
        }

        /**
         * compareTo that sorts in reverse order because we need extract-max and
         * queue provides extract-min.
         */
        public int compareTo(VertexAndWeight that) {
            if (this.active && that.active) {
                return -Double.compare(weight, that.weight);
            }
            if (this.active && !that.active) {
                return -1;
            }
            if (!this.active && that.active) {
                return +1;
            }

            // both inactive
            return 0;
        }

        @Override
        public String toString() {
            return "(" + vertex + ", " + weight + ")";
        }
    }
}