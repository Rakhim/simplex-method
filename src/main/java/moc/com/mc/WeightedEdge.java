package moc.com.mc;

public class WeightedEdge {
    private final String label;

    public WeightedEdge(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
