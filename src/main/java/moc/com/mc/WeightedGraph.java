package moc.com.mc;

import org.jgrapht.graph.DefaultUndirectedWeightedGraph;

public class WeightedGraph extends DefaultUndirectedWeightedGraph<Long, WeightedEdge> {

    public WeightedGraph() {
        super(WeightedEdge.class);
    }

    /**
     * {@inheritDoc}
     *
     * @param sourceVertex
     * @param targetVertex
     */
    @Override
    public WeightedEdge addEdge(Long sourceVertex, Long targetVertex) {
        throw new UnsupportedOperationException("Неявное создание ребер не поддерживается.");
    }
}
