package moc.com.mc;

import java.util.*;

public class GraphConnectivityHelper {

    public static boolean isConnected(int vertexCount, List<Double> weights) {
        final Map<Long, Set<Long>> adjacencyList = getAdjacencyList(vertexCount, weights);
        final Long start = 1L;
        final List<Long> marked = new ArrayList<>(vertexCount);
        marked.add(start);
        mark(adjacencyList, marked, start);
        return marked.size() == vertexCount;
    }

    public static void main(String[] args) {
        isConnected(5, Arrays.asList(
            0.2, 0.7, 0.8, 0.0,
            0.3, 0.0, 0.0,
            0.6, 0.4,
            0.5
        ));
    }

    private static Map<Long, Set<Long>> initAdjacencyList(int vertexCount) {
        final Map<Long, Set<Long>> map = new HashMap<>();
        for (long i = 1; i < vertexCount + 1; i++) {
            map.put(i, new HashSet<>());
        }
        return map;
    }

    public static Map<Long, Set<Long>> getAdjacencyList(int vertexCount, List<Double> weights) {
        final Iterator<Double> iterator = weights.iterator();
        final Map<Long, Set<Long>> map = initAdjacencyList(vertexCount);
        for (long i = 0; i < vertexCount; i++) {
            final Set<Long> neighbours = map.get(i + 1);
            for (long j = i + 1; j < vertexCount; j++) {
                final Double value = iterator.next();
                if (value > 0) {
                    neighbours.add(j + 1);
                    map.get(j + 1).add(i + 1);
                }
            }
        }

        return map;
    }

    private static void mark(Map<Long, Set<Long>> adjacencyList, List<Long> marked, Long start) {
        for (Long vertex : adjacencyList.get(start)) {
            if (!marked.contains(vertex)) {
                marked.add(vertex);
                mark(adjacencyList, marked, vertex);
            }
        }
    }

}
