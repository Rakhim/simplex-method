package moc.com;

import moc.com.matrix.MutableVector;
import moc.com.matrix.Vector;
import moc.com.matrix.VectorImpl;
import moc.com.mc.GraphConnectivityHelper;

import java.util.*;

public class ComponentHelper {

    private static void extractComponentVertexes(Set<Long> list, Map<Long, Set<Long>> adjacencyList, Long start) {
        if (adjacencyList.containsKey(start)) {
            for (Long end : adjacencyList.get(start)) {
                if (!list.contains(end)) {
                    list.add(end);
                    extractComponentVertexes(list, adjacencyList, end);
                }
            }
        }
    }

    private static Set<Long> extractComponentVertexes(Map<Long, Set<Long>> adjacencyList) {
        final Set<Long> list = new HashSet<>();
        final Long start = adjacencyList.keySet().iterator().next();
        list.add(start);
        extractComponentVertexes(list, adjacencyList, start);
        return list;
    }

    private static MutableVector extractComponent(Set<Long> componentVertexes, int vertexCount) {
        final List<Double> list = new ArrayList<>();
        for (long i = 1; i < vertexCount + 1; i++) {
            for (long j = i + 1; j < vertexCount + 1; j++) {
                list.add(componentVertexes.contains(i) ? 1.0 : 0.0);
            }
        }

        return new MutableVector(list);
    }

    /**
     * Извлекает компоненты связности
     * @param weights       -   веса ребер полного графа
     * @param vertexCount   -   количество вершин
     * @return              -   список компонент связнои (веса полных подграфов)
     */
    public static List<MutableVector> extractComponents(List<Double> weights, int vertexCount) {
        final List<MutableVector> list = new ArrayList<>();
        final Map<Long, Set<Long>> adjacencyList = GraphConnectivityHelper.getAdjacencyList(vertexCount, weights);
        while (!adjacencyList.isEmpty()) {
            final Set<Long> componentVertexes = extractComponentVertexes(adjacencyList);
            final MutableVector vector = extractComponent(componentVertexes, vertexCount);
            list.add(vector);
            for (Long vertex : componentVertexes) {
                adjacencyList.remove(vertex);
            }
        }
        return list;
    }

    public static void main(String[] args) {
        final Vector vector = new VectorImpl(Arrays.asList(0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0));
        extractComponents(vector.asList(), 20);
    }
}
