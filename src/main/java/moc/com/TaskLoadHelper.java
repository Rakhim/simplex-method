package moc.com;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import moc.com.simplexmethod.GeneralTask;
import moc.com.simplexmethod.InitialTask;

import java.io.File;
import java.io.IOException;

public class TaskLoadHelper {

    /**
     * Загрузить описание исходной задачи  из файла fileName.
     * Задача описана в формате JSON.
     *
     * @param fileName  -   имя файла
     * @return
     * @throws IOException
     */
    public static InitialTask loadInitialTask(String fileName) throws IOException {
        return new ObjectMapper().readValue(
            new File(fileName),
            new TypeReference<InitialTask>() {}
        );
    }

    /**
     * Загрузить описание задачи ЛП из файла fileName.
     * Задача описана в формате JSON.
     *
     * @param fileName  -   имя файла
     * @return
     * @throws IOException
     */
    public static GeneralTask loadTask(String fileName) throws IOException {
        return new ObjectMapper().readValue(
                new File(fileName),
                new TypeReference<GeneralTask>() {}
        );
    }
}
